
public class RaumschiffTest {

	public static void main(String[] args) {
		
		// Raumschiff 1
		
		Raumschiff r1 = new Raumschiff();
		
		r1.setPhotonentorpedoAnzahl(0);
		r1.setEnergieversorgungInProzent(0);
		r1.setSchildeInProzent(0);
		r1.setHuelleInProzent(0);
		r1.setLebenserhaltungssystemeInProzent(0);
		r1.setAndroidenAnzahl(0);
		r1.setSchiffsname(null);
		
		System.out.println("Raumschiff 1:");
		System.out.println("Anzahl Photonentorpedos: " + r1.getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + r1.getEnergieversorgungInProzent());
		System.out.println("Schilde in Prozent: " + r1.getSchildeInProzent());
		System.out.println("H�lle in Prozent: " + r1.getHuelleInProzent());
		System.out.println("Lebenserhaltungssysteme in Prozent: " + r1.getLebenserhaltungssystemeInProzent());
		System.out.println("Anzahl Androiden: " + r1.getAndroidenAnzahl());
		System.out.println("Schiffsname: " + r1.getSchiffsname());
		
		
		
		// Raumschiff 2
		
		Raumschiff r2 = new Raumschiff();
		
		r2.setPhotonentorpedoAnzahl(0);
		r2.setEnergieversorgungInProzent(0);
		r2.setSchildeInProzent(0);
		r2.setHuelleInProzent(0);
		r2.setLebenserhaltungssystemeInProzent(0);
		r2.setAndroidenAnzahl(0);
		r2.setSchiffsname(null);
		
		System.out.println("Raumschiff 2:");
		System.out.println("Anzahl Photonentorpedos: " + r2.getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + r2.getEnergieversorgungInProzent());
		System.out.println("Schilde in Prozent: " + r2.getSchildeInProzent());
		System.out.println("H�lle in Prozent: " + r2.getHuelleInProzent());
		System.out.println("Lebenserhaltungssysteme in Prozent: " + r2.getLebenserhaltungssystemeInProzent());
		System.out.println("Anzahl Androiden: " + r2.getAndroidenAnzahl());
		System.out.println("Schiffsname: " + r2.getSchiffsname());

	}

}

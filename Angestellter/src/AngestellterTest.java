
public class AngestellterTest {

	public static void main(String[] args) {
		
		Angestellter a1 = new Angestellter();
		
		//System.out.println(a1.getName());
		
		a1.setName("Max");
		a1.setGehalt(6500.50);
		a1.setAlter(25);
		
		System.out.println("Objekt a1: ");
		
		System.out.println(a1.getName());
		System.out.println(a1.getGehalt());
		System.out.println(a1.getAlter());
		
		Angestellter a2 = new Angestellter("Alex", 5000.50, 28);
		
		System.out.println("Objekt a2: ");
		
		System.out.println(a2.getName());
		System.out.println(a2.getGehalt());
		System.out.println(a2.getAlter());

	}

}

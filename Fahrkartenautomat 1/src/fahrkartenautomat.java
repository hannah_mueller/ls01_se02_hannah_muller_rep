import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        while (true) {
            fahrkartenBezahlen(fahrkartenbestellungErfassen());
        }

    }

    public static double fahrkartenbestellungErfassen () {
        int chosenType = 0;
        int ticketanzahl = 0;
        double Einzelfahrtpreis = 2.9d;
        double Tageskartenpreis = 8.6d;
        double Gruppenpreis = 23.50d;
        double zuZahlenderBetrag = 0d;
        Scanner tastatur = new Scanner(System.in);

        System.out.print("Fahrkartenbestellvorgang:\n=========================\n");

        while (chosenType != 9) {
            System.out.println("\nW�hlen Sie :\n  Einzelfahrscheine (1) \n  Tageskarten (2)\n  Gruppenkarten (3)\n  Bezahlen (9)");
            System.out.print("\nIhre Wahl: ");
            chosenType = tastatur.nextInt();

            switch (chosenType) {
                case 1:
                    System.out.print("Anzahl der Tickets: ");
                    ticketanzahl = tastatur.nextInt();
                    zuZahlenderBetrag += ticketanzahl * Einzelfahrtpreis;
                    break;
                case 2:
                    System.out.print("Anzahl der Tickets: ");
                    ticketanzahl = tastatur.nextInt();
                    zuZahlenderBetrag += ticketanzahl * Tageskartenpreis;
                    break;
                case 3:
                    System.out.print("Anzahl der Tickets: ");
                    ticketanzahl = tastatur.nextInt();
                    zuZahlenderBetrag += ticketanzahl * Gruppenpreis;
                    break;
                case 9:
                    return zuZahlenderBetrag;
                default:
                    System.out.println(">>falsche Eingabe<<");
                    break;
            }
        }
        return zuZahlenderBetrag;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);

        // Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0d;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            //System.out.println("ticketanzahl" + ticketanzahl);
            System.out.println("Noch zu zahlen: " + String.format("%1.2f",zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;

        }
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

        return eingezahlterGesamtbetrag - zuZahlenderBetrag;

    }

    public static void fahrkartenAusgeben (double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) {
        String currentTask = "Fahrschein wird ausgegeben";
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\n" + currentTask);
        for (int i = 0; i < currentTask.length(); i++)
        {
            System.out.print("=");

            warte(100);
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\nWir w�nschen Ihnen eine gute Fahrt.\n");


    }

    public static void warte(int millisekunde) {
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void muenzeAusgeben(int betrag, String einheit) {
        System.out.println(betrag + " " + einheit);
    }


    public static double rueckgeldAusgeben (double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) {
        double r�ckgabebetrag = 0;

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
            System.out.println("Der R�ckgabebetrag in H�he von " + String.format("%1.2f", r�ckgabebetrag) + " EURO");
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
            {
                muenzeAusgeben(2,"Euro");
                r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                muenzeAusgeben(1, "EURO");
                r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                muenzeAusgeben(50, "CENT");
                r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                muenzeAusgeben(20, "CENT");
                r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                muenzeAusgeben(10, "CENT");
                r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                muenzeAusgeben(5, "CENT");
                r�ckgabebetrag -= 0.05;
            }
            return r�ckgabebetrag;
        }
        return r�ckgabebetrag;
    }
}

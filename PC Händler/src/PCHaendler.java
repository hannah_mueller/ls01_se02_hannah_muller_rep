import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		String erg = myScanner.next();
		return erg;
	
	}
	public static int liesInt(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		int erg = myScanner.nextInt();
		return erg;
	}
	
	

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("Was m�chten Sie bestellen?");

		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}

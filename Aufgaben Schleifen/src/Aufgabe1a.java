import java.util.Scanner;
public class Aufgabe1a {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl ein: ");
		int eingabeZahl = myScanner.nextInt();
		
		int zaehler = 1;
		
		while (zaehler <= eingabeZahl) {
			System.out.print(zaehler + " ");
			zaehler++;
		}

	}
	
}

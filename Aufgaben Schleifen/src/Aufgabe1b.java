import java.util.Scanner;
public class Aufgabe1b {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl ein: ");
		int eingabeZahl = myScanner.nextInt();

		while (eingabeZahl >= 1) {
			System.out.print(eingabeZahl + " ");
			eingabeZahl--;
		}

	}

}

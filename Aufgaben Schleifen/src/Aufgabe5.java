import java.util.Scanner;
public class Aufgabe5 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		//Eingabe
		System.out.println("Laufzeit (in Jahren) des Sparvertrags: ");
		
		int laufzeit = myScanner.nextInt();
		
		System.out.println("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		
		double kapital = myScanner.nextInt();
		
		System.out.println("Zinssatz: ");
		
		double zinssatz = myScanner.nextInt();
		
		
		double p;
		double endkapital;
		
		
		//Rechnung
		p = 1 + zinssatz / 100;
		endkapital = kapital * Math.pow(p, laufzeit);    //Berechnung
		endkapital = Math.rint(endkapital * 100);        //Rundung
		endkapital = endkapital / 100;
		
		
		//Ausgabe
		System.out.println("Eingezahltes Kapital: " + kapital + " Euro");
		System.out.println("Ausgezahltes Kapital: " + endkapital + " Euro");
		
	}
}
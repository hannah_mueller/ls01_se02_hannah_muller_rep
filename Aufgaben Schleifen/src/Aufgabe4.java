import java.util.Scanner;
public class Aufgabe4 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte den Startwert in Celsius eingeben: ");
		
		double startwert = myScanner.nextInt();
		
		System.out.println("Bitte den Endwert in Celsius eingeben: ");
		
		double endwert = myScanner.nextInt();
		
		System.out.println("Bitte die Schrittweise in Grad Celsius eingeben: ");
		
		int schrittweite = myScanner.nextInt();
		
		double cwert = startwert;
		double fwert = 0;
		
		while(cwert <= endwert) {
			fwert = cwert * 1.8 + 32;
			
			System.out.println(cwert + " " + fwert);
			cwert = cwert + schrittweite;
			
		}
		
	}

}

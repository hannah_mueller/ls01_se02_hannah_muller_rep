import java.text.SimpleDateFormat;
import java.util.Date;

public class Benutzer {
	
	private int benutzernummer;
	private String vorname;
	private String nachname;
	private int geburtsjahr;
	
	public Benutzer() {
		this.benutzernummer = 0;
		this.vorname = null;
		this.nachname = null;
		this.geburtsjahr = 0;
	}
	
	public Benutzer(int benutzernummer, String vorname, String nachname, int geburtsjahr) {
		this.benutzernummer = benutzernummer;
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsjahr = geburtsjahr;
	}
	
	public void setBenutzernummer(int benutzernummer) {
		this.benutzernummer = benutzernummer;
	}
	
	public int getBenutzernummer() {
		return benutzernummer;
	}
	
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	
	public String getNachname() {
		return nachname;
	}
	
	public void setGeburtsjahr(int geburtsjahr) {
		this.geburtsjahr = geburtsjahr;
	}
	
	public int getGeburtsjahr() {
		return geburtsjahr;
	}
	
	public int getAlter() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		Date date = new Date();
		int aYear = Integer.parseInt(formatter.format(date));
		int alter = aYear - geburtsjahr;
		return alter;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.benutzernummer + "Nachname: " + this.nachname;
	}
}
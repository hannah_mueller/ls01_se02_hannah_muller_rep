import java.util.Scanner;

public class fahrkartenautomat {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
        while (true) {
            fahrkartenBezahlen(fahrkartenbestellungErfassen());

        }

	}
	
    public static double fahrkartenbestellungErfassen () {

        //Initialisierung von Variablen
        int chosenType = 0;
        int ticketanzahl = 0;
        double zuZahlenderBetrag = 0d;

        //Intitialiserung von Arrays
        String[] Bezeichnungen = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin ABC"};
        double[] Preise = {2.9d, 3.3d, 3.6d, 1.9d, 8.6d, 9d, 9.6d, 23.5d, 24.3d, 24.9d};

        //Initialisierung von Scanner
        Scanner tastatur = new Scanner(System.in);

        //Ausgabe von Auswahlm�glichkeiten
        System.out.print("Fahrkartenbestellvorgang:\n=========================\n");
        while (chosenType != Bezeichnungen.length + 1) {
            for (int i = 0; i < Bezeichnungen.length; i++) {
                System.out.println("("+ (i+1) + ")" + Bezeichnungen[i]);
            }
            //Ausgabe von Bezahlen
            System.out.println("(" + (Bezeichnungen.length + 1) + ")" + "Bezahlen");
            System.out.print("Ihre Wahl: ");
            chosenType = tastatur.nextInt();

            //Abfrage ob Chosentype ein Element des Arrays ist
            if (chosenType <= Bezeichnungen.length && chosenType > -1) {
                //Abfrage der Ticketanzahl
                System.out.print("Anzahl der Tickets: ");
                ticketanzahl = tastatur.nextInt();
                //Berechnung des Preises
                zuZahlenderBetrag += ticketanzahl * Preise[chosenType-1];
            }
            //Abfrage ob Bezahlen ausgew�hlt wurde
            else if (chosenType == Bezeichnungen.length + 1){
                return zuZahlenderBetrag;
            }
            //Ansonsten falsche Eingabe
            else {
                System.out.println(">>falsche Eingabe<<");
            }
        }

        return zuZahlenderBetrag;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);

        // Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0d;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            //System.out.println("Ticketanzahl" + Ticketanzahl);
            System.out.println("Noch zu zahlen: " + String.format("%1.2f",zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;

        }
        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

        return eingezahlterGesamtbetrag - zuZahlenderBetrag;

    }

    public static void fahrkartenAusgeben (double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) {
        String currentTask = "Fahrschein wird ausgegeben";
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\n" + currentTask);
        for (int i = 0; i < currentTask.length(); i++)
        {
            System.out.print("=");

            warte(100);
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\nWir w�nschen Ihnen eine gute Fahrt.\n");


    }

    public static void warte(int millisekunde) {
        try {
            Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void muenzeAusgeben(int betrag, String einheit) {
        System.out.println(betrag + " " + einheit);
    }


    public static double rueckgeldAusgeben (double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) {
        double r�ckgabebetrag = 0;

        // R�ckgeldberechnung und -ausgabe
        // -------------------------------
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
            System.out.println("Der R�ckgabebetrag in H�he von " + String.format("%1.2f", r�ckgabebetrag) + " EURO");
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
            {
                muenzeAusgeben(2,"Euro");
                r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                muenzeAusgeben(1, "EURO");
                r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                muenzeAusgeben(50, "CENT");
                r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                muenzeAusgeben(20, "CENT");
                r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                muenzeAusgeben(10, "CENT");
                r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                muenzeAusgeben(5, "CENT");
                r�ckgabebetrag -= 0.05;
            }
            return r�ckgabebetrag;
        }
        return r�ckgabebetrag;
    }
}
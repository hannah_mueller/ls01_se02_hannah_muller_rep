import java.util.Scanner;  // 1
public class Scannerbeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);  // 2
		
		double dzahl = myScanner.nextDouble();  // 3
		
		boolean b = myScanner.nextBoolean();
		
		float f = myScanner.nextFloat();
		
	}

}

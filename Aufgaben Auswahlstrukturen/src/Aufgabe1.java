
public class Aufgabe1 {

	public static void main(String[] args) {
	
		vergleichen1(7,16);
		vergleichen2(8,2);
		vergleichen3(17,85);
		
	}

	public static void vergleichen1(int zahl1, int zahl2) {
		if(zahl1 == zahl2) {
		
			System.out.println("Die Zahlen sind gleich.");
		}
		else {
			
			System.out.println("Die Zahlen sind ungleich.");
		}
	}
	
	public static int vergleichen2(int zahl1, int zahl2) {
		if(zahl2 > zahl1) {
			
			System.out.println("Die zweite Zahl ist gr��er, als die erste.");
			return zahl2;
		}
		else {
			
			System.out.println("Die erste Zahl ist gr��er, als die zweite.");
			return zahl1;
		}
	}
	
	public static int vergleichen3(int zahl1, int zahl2) {
		if(zahl1 >= zahl2) {
			
			System.out.println("Die erste Zahl ist gr��er oder gleich.");
			return zahl1;
		}
		else {
			
			System.out.println("Die zweite Zahl ist gr��er oder gleich.");
			return zahl2;
		}
	}
}
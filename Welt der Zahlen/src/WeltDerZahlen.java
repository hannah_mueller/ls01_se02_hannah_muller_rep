/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 27.10.2020
  * @author << Hannah M�ller >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = (long) 3E11 ;
    
    // Wie viele Einwohner hat Berlin?
     int  bewohnerBerlin = (int) 3769E3 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      int alterTage = 7174 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
      long gewichtKilogramm = (long) 2E5 ;
    
    // Schreiben Sie auf, wie viele km² das größte Land der Erde hat?
      long flaecheGroessteLand = 17098240 ;
    
    // Wie groß ist das kleinste Land der Erde?
    
      int flaecheKleinsteLand = 1 ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Anzahl der Sterne:" + anzahlSterne);
    System.out.println("Einwohner Berlins:" + bewohnerBerlin);
    System.out.println("Mein Alter:" + alterTage);
    System.out.println("Gewicht des schwersten Tieres:" + gewichtKilogramm);
    System.out.println("Fl�che des gr��ten Landes:" + flaecheGroessteLand);
    System.out.println("Fl�che des kleinsten Landes:" + flaecheKleinsteLand);
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}



public class LadungTest {
	
	public static void main(String[] args) {
		
		Ladung l1 = new Ladung();
		
		l1.setBezeichnung("Torpedos");
		l1.setMenge(25);
		
		System.out.println("Ladung l1:");
		System.out.println(l1.getBezeichnung());
		System.out.println(l1.getMenge());
		
		
		Ladung l2 = new Ladung("Torpedos", 30);
		
		System.out.println("Ladung l2:");
		System.out.println(l2.getBezeichnung());
		System.out.println(l2.getMenge());
		
	}

}


public class AuswahlBeispiele {

	public static void main(String[] args) {
		
		int z1 = 5;
		int z2 = 7;
		
		int erg1 = min(z1,z2);
		int erg2 = max(z1,z2);
		double erg3 = max2(2.5, 7.2, 3.6);
		
		System.out.println("Ergebnis: " + erg1);
		System.out.println("Ergebnis: " + erg2);
		System.out.println("Ergebnis: " + erg3);
	}

	public static int min(int zahl1, int zahl2) {
		int erg1;
		if (zahl1 < zahl2) {
			erg1 = zahl1;
		}
		else {
			erg1 = zahl2;
		}
		return erg1;
	}
	
	public static int max(int zahl1, int zahl2) {
		int erg2;
		if (zahl1 > zahl2) {
			erg2 = zahl1;
		}
		else {
			erg2 = zahl2;
		}
		return erg2;
	}
	
	public static double max2(double zahl1, double zahl2, double zahl3) {
		double erg3;
		if (zahl1 > zahl2) {
			erg3 = zahl1;
		}
		if (zahl1 > zahl3) {
			erg3 = zahl1;
		}
		if (zahl2 > zahl1) {
			erg3 = zahl2;
		}
		if (zahl2 > zahl3) {
			erg3 = zahl2;
		}
		if (zahl3 > zahl1) {
			erg3 = zahl3;
		}
		if (zahl3 > zahl2) {
			erg3 = zahl3;
		}
		else {
			erg3 = zahl2;
		}
		return erg3;
	}
}
